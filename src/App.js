import "./styles.css";
import React from "react";
import { useEffect, useState } from "react";

var team_players_global = [];
// comment
/*
  NBA Players
  Implement a React component that:
    1. Shows the list of players of each team.

  Example: Following the data from getPlayers service, the result should be as follows:
  ============================
  Team: Lakers
    - LeBron James
    - Anthony Davis
    - Thomas Bryant
  Team: Celtics
    - Jabari Bird
    - Michael Smith
  Team: Pistons
    - Zach Lofton
    - Keenan Evans
  ============================

  Note:
    - Consider getPlayers service as it was a real backend endpoint.
*/

const Services = {
  getPlayers() {
    const data = [
      { name: "LeBron", lastName: "James", weight: 100, teamName: "Lakers" },
      { name: "Thomas", lastName: "Bryant", weight: 100, teamName: "Lakers" },
      { name: "Zach", lastName: "Lofton", weight: 270, teamName: "Pistons" },
      { name: "Anthony", lastName: "Davis", weight: 100, teamName: "Lakers" },
      { name: "Jabari", lastName: "Bird", weight: 230, teamName: "Celtics" },
      { name: "Keenan", lastName: "Evans", weight: 170, teamName: "Pistons" },
      { name: "Michael", lastName: "Smith", weight: 100, teamName: "Celtics" },
    ];
    return Promise.resolve(data);
  },
};

function ListTeam(props) {
  return (
    <a href="#" class="list-group-item list-group-item-action" aria-current="true">
      <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">Team: { props.value.team }</h5>
        <small>{ props.value.players.length }</small>
      </div>
      {
        props.value.players.map( p => <p class="mb-1">Player: { p.name } { p.lastname }</p>)
      }
    </a>
  )
}

export const NBAPlayers = (props) => {
  const [ teamsPlayers, setPlayers ] = useState([]);

  const numbers = props.numbers;

  // setPlayers([1,2,3]);
  useEffect( () => {

    async function getPlayers() {
      const team_players =  await getTeamsPlayers();
      // console.log(typeof team_players);
      return team_players;
      // setPlayers(team_players);
    };

    // console.log('use effect');
    const team_players = getPlayers();
    
    team_players.then( tp => { 
      // console.log(tp);
      setPlayers(tp);
      // tp.map( (value, key) => {
      //   console.log(value.players);
      // });

    });
    
  });

  const listItems = teamsPlayers.map( (value, key) => <ListTeam key={key.toString()} value={value} />);

  return (
    <div>
      <div class="list-group">
      <h1>NBA Players</h1>

          { listItems }
      </div>
     </div>
  );

};

export default function App() {
  console.log('*');

  const numbers = [1,2,3,4];
    return (
      <div className="App">
        <div class="row">
          <div class="col">
            <NBAPlayers numbers={ numbers }/>
          </div>
        </div>
      </div>
    );



}


const getTeamsPlayers = async () => {

  const players = await Services.getPlayers();
  // console.log (players);

  const teams = players.map(p => p.teamName);
  let teams_noduplicate = [];
  teams.forEach( (team1) => {
    const found = teams_noduplicate.find( team2 => team1 === team2);
    if (found === undefined) teams_noduplicate.push(team1);
  });
  // console.log('teamsnoduplicate', teams_noduplicate);

  let teams_players =[];

  teams_noduplicate.forEach( (t) => {
    const playersofteam = players.filter( p => p.teamName === t);
    let team_players = {
      'team' : t,
      'players' : playersofteam
    }

    teams_players.push(team_players);
  });

  // console.log(teams_players);

  return teams_players;
  /*
  Services.getPlayers().then ( (players) => {
    console.log('list', players);

    // let teams = [];
    
  // console.log(teams);
    const teams = players.map(p => p.teamName);
    let teams_noduplicate = [];
    teams.forEach( (team1) => {
      const found = teams_noduplicate.find( team2 => team1 === team2);
      if (found === undefined) teams_noduplicate.push(team1);
    });
    console.log('teamsnoduplicate', teams_noduplicate);

    let teams_players =[];

    teams_noduplicate.forEach( (t) => {
      const playersofteam = players.filter( p => p.teamName === t);
      let team_players = {
        [ t ]: playersofteam
      }
      teams_players.push(team_players);
    });

    console.log(teams_players);

  });
  */
;}


